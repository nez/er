### er

**example:**
```go
import (
	"io/ioutil"
	. "codeberg.org/nez/er"
)

func main() {
        _, err := ioutil.ReadFile("_")
        Fatal(err)
}
```
