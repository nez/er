package er

import (
    "log"
)

func Panic(e error) {
	if e != nil {
		log.Panicln(e)
	}
}

func Fatal(e error) {
	if e != nil {
		log.Fatalln(e)
	}
}

func Print(e error) {
	if e != nil {
		log.Println(e)
	}
}

func Recover() {
	if r := recover(); r != nil {
		log.Println(r)
	}
}
